<?php

namespace App\Controller;

use App\Entity\FileProducts;
use App\Forms\FileProductType;
use App\Repository\FileProductsRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Response;

class PlateformFileController extends AbstractController
{
    /**
     * @Route("/", name="files", methods={"GET","HEAD"})
     */
    public function filesGet(FileProductsRepository $repository)
    {
        // On récupère les fichiers avec un findAll()
        $files = $repository->findAll();
        // On affiche le nombre de fichiers
        $fileCount= count($files);

        // On envoie les données récupérées, dans le fichier .twig servant de front
        return $this->render('home/index.html.twig',[
                            "count" => $fileCount,
                            "files" => $files]);
    }

     /**
     * @Route("/plateformFiles/add", name="app_file_new")
     */
    public function filesAdd(Request $request, EntityManagerInterface $entityManager)
    {       
        
        
        $file = new FileProducts(); // On initialise un nouveau object FileProducts()
        //on affecte l'id de l'utilisateur au fichier

        $formadd = $this->createFormBuilder($file)
        ->add('name', TextType::class)
        ->add('description', TextareaType::class)
        ->add('upload_date', DateType::class)
        ->getForm();
        $form = $this->createForm(FileProductType::class, $file); // On initialise un formulaire 
                                                                    //que l'on lie à FileProducts()
        $form->handleRequest($request); // On vérifie que le formulaire soit bien remplie et on le préapare 
        $formadd->handleRequest($request);  // à être bien envoyé en base de donnée 
        

        
        if ($form->isSubmitted() && $form->isValid()) { // On intègre des vérifications supplémentaire pour éviter 
                                                        // les injections frauduleuses
            /** @var UploadedFile $brochureFile */

            $brochureFile = $form->get('brochure')->getData(); 

            //Cette condition est nécessaire car le champ "brochure" n'est pas obligatoire
            // donc le fichier PDF ne doit être traité que lors du téléchargement d'un fichier
            if ($brochureFile) {
                $originalFilename = pathinfo($brochureFile->getClientOriginalName(), PATHINFO_FILENAME);
               // ceci est nécessaire pour inclure en toute sécurité le nom de fichier dans l'URL
                //$safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                
                //génère un unique nom 
                $safeFilename = md5($originalFilename); 
                // on laisse symfony deviner la déclinaison du fichier, pour éviter les injections frauduleuses -> guessExtension() 
                // on procède à la concaténation du nom du fichier
                $newFilename = $safeFilename.'-'.uniqid().'.'.$brochureFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    // on essaye de trnasférer le fichier dans public/upload avec 
                    // son nouveau nom
                    $brochureFile->move(
                        $this->getParameter('brochures_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    $e->getMessage();
                }
                
                
                // met à jour la propriété 'brochureFilename' pour stocker le nom du fichier PDF
                $file->setBrochureFilename($newFilename);
            } 

            
                //on récupère les données du formulaire
                //et on hydrate notre advert
                $file = $form->getData();
    
                //pour inscrire l'objet en base de données
                //il faut faire appel à l'EntityManager
                //On doit d'abord le récuperer
                $entityManager = $this->getDoctrine()->getManager();
    
                //on demande à l'Entity Manager d'écrire l'objet dans la base
                $entityManager->persist($file);
                //on demande à l'entity Manager de nettoyer derrière lui
                $entityManager->flush();
            }
            
            // on affiche un message pour indiquer que le fichier est bien 
            // enregistré 
            $this->addFlash('sucess','Fichier bien enregisté');
        
        return $this->render('home/add.html.twig',[
            'form' => $form->createView(), // on envoie le formualaire dans le template
            'formadd' => $formadd->createView() 
        ]);
        }

    /**
     * @Route("/plateformFiles/{id}", name="files_view", methods={"GET","POST"})
     */

    public function filesView( $id): Response
    {
        // on récupère le repository FileProducts, et on filtre par id 
       $file = $this->getDoctrine()->getRepository(FileProducts::class)->find($id);

       if ($file){
           // on affiche le fichier selon son Id
           return $this->render('home/show.html.twig',[
               'file' => $file]);
       } else {
           // si aucun Id est trouvé on renvoie l'erreur suivante
           $this->addFlash(
               "error_message","Aucun fichier trouvé pour cet Id  ");
               return $this->render('home/notFound.html.twig',[
                'files' => $file]);
       }
    }

    /**
     * @Route("/plateformFiles/{id}", name="files_delete", requirements={
     *  "id" = "\d+"
     * })
     */
    public function filesDelete($id)
    {
        //TODO : Implémentation des users
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        // //on récupère l'utilisateur
        // $user = $this->getUser();

        //on va chercher l'annonce à supprimer
        $fileRepository = $this->getDoctrine()->getRepository(FileProducts::class);
        $file = $fileRepository->find($id);

        //si l'auteur de l'annonce n'est pas 
        //l'utilisateur actuel
        // if ($file->getAuthor() != $user->getUsername()){
        //     //on redirige vers l'annonce 
        //     return $this->redirectToRoute('advert_view',
        //     [
        //         'id' => $file->getId()
        //     ]);
        // }

        //si l'annonce existe
        if (($file)){
            //on la supprime 
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($file);
            $entityManager->flush();
            
            $this->addFlash('success', 'Annonce supprimée.');
        }
        //on redirige ensuite vers l'accueil
        return $this->redirectToRoute('files');
    }
    

     /**
     * @Route("/plateformFiles/{id}", name="files_edit", methods={"PUT"}, requirements={
     *  "id" = "\d+"
     * })
     */
    public function filesEdit(int $id, Request $request, EntityManagerInterface $entityManager)
    {


        // $file = $this->getDoctrine()->getRepository(File::class)->find($id);
        
        // if ($file){
        //     $data = json_decode($request->getContent(), true);
            
        //     $normalizer = new ObjectNormalizer();
            
        //     $file = $normalizer->denormalize($data, File::class, 'json', ['object_to_populate' => $file ]);

        //     $em->persist($file);
        //     $em->flush();

        //     return $this->json($file);

        //  } else {
        //     return $this->json([
        //         "error_message" => "No file found with this id"
        //     ], 404);
        //  }
        // $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        // $user= $this->getUser();


        $fileRepository=$this->getDoctrine()->getRepository(FileProducts::class);
        $file = $fileRepository->find($id);


        if ($file){
            return $this->redirectToRoute('files_view',
            [
                'id' => $file->getId()
            ]);

        }

        if (!is_null($file)){

            $form = $this->createFormBuilder($file)
            ->add('name', TextType::class)
            ->add('upload_date', DateTime::class)
            ->add('path', __FILE__)
            ->add('author', TextType::class)
            ->add('submit', SubmitType::class, ['label' => 'Sauvegarder'])
            ->getForm();
    
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            
    
            $file = $form->getData();
    
            
            $entityManager = $this->getDoctrine()->getManager();
    
            $entityManager->persist($file);
    
            $entityManager->flush();

            $this->addFlash('sucess','Annonce modifiée avec succès');


            return $this->redirectToRoute('files_view', 
            ['id' => $file->getId()]
            );
            }

        } else {
            return $this->render('home/notFound.html.twig');
        }
        return $this->render('home/edit.html.twig',
        [
            'file_form'=>$form->createView()
        ]);
    }

     }


