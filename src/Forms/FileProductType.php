<?php

namespace App\Forms;

use App\Entity\FileProducts;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;


class FileProductType extends AbstractType
{
    // Permet d'obtenir le widget de téléchargement de fichier, atsuce : 
    // ajouter le champ du formulaire comme "non mappé", afin que Symfony 
    // n'essaye pas d'obtenir sa valeur à partir de l'entité associée:

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ...
            ->add('brochure', FileType::class, [
                'label' => 'Insérez un fichiers',

                // non mappé signifie que ce champ n'est associé à aucune propriété d'entité
                'mapped' => false,

                // le rendre facultatif pour que vous n'ayez pas à télécharger à nouveau le fichier 
                // chaque fois que vous modifiez les détails du produit
                'required' => false,

                // les champs non mappés ne peuvent pas définir leur validation à l'aide d'annotations
                // dans l'entité associée, nous pouvons donc utiliser les classes de contraintes PHP
                'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            'image/*', //Permet de télécharger la plupart des images
                            'application/pdf', //Permet de télécharger la plupart
                            'application/x-pdf',// des PDF
                        ],
                        'mimeTypesMessage' => 'Entrez un fichier téléchargeable',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FileProducts::class,
        ]);
    }
}



?>